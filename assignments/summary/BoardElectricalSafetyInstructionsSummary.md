**Board Electrical Safety Instructions**
 
**What is Electrical Safety ?**

![ALT](https://cdn.shopify.com/s/files/1/0091/7238/5851/products/ELESI1076_300x300.jpg?v=15428934559)

 - Electrically powered equipment can pose a significant hazard to workers, particularly when mishandled or not maintained.  Many electrical devices have high voltage or high power requirements, carrying even more risk.

**Power Supply**
 
 - The primary goal of safety standards for power supplies used in electrical equipment is to protect against fire, electric shock and injury.... etc so we have to follow some steps in do's and don’ts to overcome these problems

**Do’s:-**

- Always make sure that the output voltage of the power supply matches the input voltage of the board.
- While turning on the power supply make sure every circuit is connected properly.

**Don’ts :-**

 ![ALT](https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSNo-nHya2ArRAU7NJmgaisu1q_e7D0iuvbTQ&usqp=CAU)

- Do not connect power supply without matching the power rating.
- Never connect a higher output(12V/3Amp) to a lower (5V/2Amp) input .
- Do not try to force the connector into the power socket ,it may damage the connector.



**Handling**

**Do’s :-**

- Treat every device like it is energized, even if it does not look like it is operational.
- While working keep the board on a flat stable surface (wooden table) .
- Unplug the device before performing any operation on them.
- When handling electrical equipment, make sure your hands are dry.
- Keep all electrical circuit contact points enclosed.
- If the board becomes too hot try to cool it with a external usb fan .

**Don’ts :-**

![ALT](https://media.tenor.com/images/19a129a68c133dd3d5b9dbeeb563e4f6/tenor.gif)

- Don’t handle the board when its powered ON.
- Never touch electrical equipment when any part of your body is wet, (that includes fair amounts of perspiration).
- Do not touch any sort of metal to the development board.
- Disconnect the power source before servicing or repairing electrical equipment.


**GPIO**

![ALT](https://gitlab.com/balanaguyashwanth/iotmodule2/-/raw/master/assignments/summary/Assets/gpio.jpeg)

GPIO stands for General Purpose Input/Output and it's a standard interface used to connect microcontrollers to other electronic devices.

**Do’s :-**

- Find out whether the board runs on 3.3v or 5v logic.
- Always connect the LED (or sensors) using appropriate resistors .
- To Use 5V peripherals with 3.3V we require a logic level converter.


**Don’ts :-**

- Never connect anything greater that 5v to a 3.3v pin.
- Avoid making connections when the board is running.
- Don't plug anything with a high (or negative) voltage.
- Do not connect a motor directly , use a transistor to drive it .



**Guidelines for using interfaces(UART,I2C,SPI)**

**UART**

![ALT](https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcR16OMdxZiCGVRSEWCcUbAPPwgFZ-bcQPLY7g&usqp=CAU)

  A universal asynchronous receiver/transmitter (UART) is a simple serial communication protocol that allows the host communicate with the other device. Essentially, the UART acts as an intermediary between parallel and serial interfaces.


- Connect Rx pin of device1 to Tx pin of device2 ,similarly Tx pin of device1 to Rx pin of device2.
- If the device1 works on 5v and device2 works at 3.3v then use the level shifting mechanism(voltage divider )
- Genrally UART is used to communicate with board through USB to TTL connection . USB to TTL connection does not require a protection circuit .
- Whereas Senor interfacing using UART might require a protection circuit.


**I2C**

![ALT](https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSxPbuCoUKDmd4nZAPJrPY0nVn9R1HD20AJrQ&usqp=CAU)

It is bidirectional communication between chips and cpu and it requires two buses to communicate SDA and SCL 
It is short range and having low peripherals

- while using I2c interfaces with sensors SDA and SDL lines must be protected.
- Protection of these lines is done by using pullup registers on both lines.
- If you use the inbuilt pullup registers in the board you wont need an external circuit.
- if you are using bread-board to connect your sensor , use the pullup resistor .
- Generally , 2.2kohm <= 4K ohm resistors are used.


**SPI**

![ALT](https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQfQGg5DyTdjf0GkyJc1e9y9nQ1lvaV25iAyw&usqp=CAU)

SPI stands for Serial Peripheral Interface. It is a synchronous serial communication interface specification used for short distance communication, primarily in embedded systems.

- Generally ,Spi in development boards is in Push-pull mode.
- Push-pull mode does not require any protection circuit.
- on Spi interface if you are using more than one slaves it is possible that the device2 can "hear" and "respond" to the master's communication with device1- which is an disturbance .
- To overcome this problem , we use a protection circuit with pullup resistors on each the Slave Select line(CS).
- Resistors value can be between 1kOhm ~10kOhm . Generally 4.7kOhm resistor is used.



