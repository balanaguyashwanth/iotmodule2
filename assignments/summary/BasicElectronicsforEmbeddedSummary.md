***Basic Electronics for Embedded Summary***

**Sensor :-**

 - Sensor is a device used for the converts of physical action or characteristics into the electrical signals. 
 - This is a hardware device that takes the input from environment and gives to the system by converting it.
 - For example, a thermometer takes the temperature as physical characteristic and then converts it into electrical signals for the system.

**Actuator :-**

 - Actuator is a device that converts the electrical signals into the physical action or characteristics.
 - It takes the input from the system and gives output to the environment.
 - For example, motors and heaters are some of the commonly used actuators.

**In simple terms we can say** 

 - A sensor is a transducer. A transducer is any physical device that converts one form of energy into another. In simple terms, an actuator operates in the reverse direction of a sensor. ... It takes an electrical input and turns it into physical action.

 ![ALT](https://bridgera.com//wp-content/uploads/2017/06/sensor_actuator_graphicssec1_pg16.jpg)

**Analog and Digital signals**

![ALT](https://gitlab.com/balanaguyashwanth/iotmodule2/-/raw/master/assignments/summary/Assets/Analog-and-Digital.png)

 - **Analog and Digital signals** are the types of signals carrying information by using electrical energy.
 - The major difference between both signals is that the analog signals that have a continuous electrical.
 - while digital signals non-continuous electrical it means discrete signals. 


**Analog Signals :-**

 - The analog signals were used in many systems to produce signals to carry information.  
 - In short, to understand the analog signals –  all signals that are natural or come naturally are analog signals like sound waves of human voice etc.

**Digital Signals :-**

 - These signals are represented by binary numbers and consist of different voltage values.

**Difference Between Analog and Digital Signal :-**

![ALT](https://gitlab.com/balanaguyashwanth/iotmodule2/-/raw/master/assignments/summary/Assets/analog_and_digital_comparision.png)

**Microcontroller vs Microprocessor**

![ALT](https://components101.com/sites/default/files/components/Difference-between-Microprocessor-and-Microcontroller.jpg)

**Microcontroller :-**

 - A microcontroller is a small and low-cost microcomputer.
 - which is designed to perform the specific tasks of embedded systems like displaying microwave’s information, receiving remote signals, etc.
 - The general microcontroller consists of the processor, the memory (RAM, ROM, EPROM), Serial ports, peripherals (timers, counters), etc.
 - Microcontroller can do only one specific task at a time.
 - For example:- Cameras, Robots, Washing Machine, Microwave Ovens etc

**Microprocessor :-**

 - A microprocessor is an small chip capable of performing ALU that is used by a computer to do its work.
 - It can do many tasks at a time.
 - But it only consists of single integrated circuit chip containing millions of very small components including transistors, resistors, and diodes that work together.
 - It can do more speed compare to microcontroller.
 - For example:- cars, smartphones, laptops etc.  

 ![ALT](https://forelectronics.files.wordpress.com/2014/05/difference.jpg)


**What is a Raspberry Pi ?**

 - The Raspberry Pi is a low cost, small sized computer that runs Linux it has support to plugs into a computer monitor or TV, and uses a standard keyboard and mouse. 
 - It is a capable little device that enables people of all ages to explore computing, and to learn how to program in languages like Scratch and Python. 
 - It’s capable of doing everything you’d expect a desktop computer to do, from browsing the internet and playing high-definition video, to making spreadsheets, word-processing, and playing games etc.
 - It also provides a set of GPIO (general purpose input/output) pins that allow you to control electronic components and explore the Internet of Things 
 - They are so many generation models in Raspberry pi like Pi 1, Pi 2,Pi 3,Pi 3 Model B+ and Pi 3 Model A+ etc according the features and price the model will various.

![ALT](https://www.raspberrypi.org/homepage-9df4b/static/raspberry-pi-4-labelled-2857741801afdf1cabeaa58325e07b58.png)

**Main features and components in Raspberry pi :-**
 
 - Mini Computer.
 - Limited but large power for its size.
 - No storage.
 - It is a SOC (System On Chip).
 - Microprocessor.
 - Can load a linux OS on it.
 - Pi uses ARM
 - Connect to sensors or actuators.
 - GPU (Graphics Processing Unit)
 - UART.
 - 3.5 MM Jack, HDMI Audio Out.

**Raspberry Pi Interfaces :-**

 - GPIO
 - UART
 - SPI
 - I2C
 - PWM

**Serial and Parallel communication :-**

![ALT](https://gitlab.com/balanaguyashwanth/iotmodule2/-/raw/master/assignments/summary/Assets/serial_and_parallel.jpg)

**What is Serial communication ?**

 - In serial communication the data bits are transmitted serially over a common communication link one after the other.
 - Basically it does not allow simultaneous transmission of data because only a single channel is utilized. 


**What is parallel communication ?**

 - In parallel communication the various data bits are simultaneously transmitted using multiple communication links between sender and receiver.
 - Here, despite using a single channel between sender and receiver, various links are used and each bit of data is transmitted separately and independently over all the communication link.
 
**Serial communication Interfaces :-**

**What is SPI ?**

 ![ALT](https://gitlab.com/balanaguyashwanth/iotmodule2/-/raw/master/assignments/summary/Assets/SPI-interface-diagram.jpg)

 - SPI stands for Serial Peripheral Interface. Like I2C, SPI is a different form of serial-communications protocol specially designed for microcontrollers to talk to each other. 
 - SPI allows a single master device with a maximum of four slave devices.
 - SPI is typically much faster than I2C due to the simple protocol and, while data/clock lines are shared between devices, each device requires a unique address wire.
 -  SPI is commonly found in places where speed is important such as with SD cards and display modules, or when information updates and changes quickly, like with temperature sensors.

**What is UART ?**

 ![ALT](https://gitlab.com/balanaguyashwanth/iotmodule2/-/raw/master/assignments/summary/Assets/UART-interface-diagram.jpg)

 - UART Stands for Universal Asynchronous Reception and Transmission (UART)
 - Simple serial communication protocol that allows the host communicate with the other device.
 - UART supports bidrectional, asynchronous and serial data transmission.
 - It has two data lines, one to transmit (TX) and another to receive (RX) which is used to communicate through digital pin 0, digital pin 1.
 - TX and RX are connected between two devices. (eg. USB and computer)
 - UART can also handle synchronization management issues between computers and external serial devices.
 

**What is I2C ?**

![ALT](https://gitlab.com/balanaguyashwanth/iotmodule2/-/raw/master/assignments/summary/Assets/I2C-interface-diagram.jpg)

 - It is a serial communication protocol. It provides good support to the slow devices, for example, EEPROM, ADC, and RTC etc.
 - It are not only used with the single board but also used with the other external components which have connected with boards through the cables.
 - It is basically a two-wire bidirectional communication protocol.In which one wire is used for the data (SDA) and other wire is used for the clock (SCL).
 - In this master able to send and receive the data from the slave. The clock bus is controlled by the master.


**Parallel communication Interfaces :-**

**What is GPIO ?**

![ALT](https://www.electronicwings.com/public/images/user_images/images/Raspberry%20Pi/RaspberryPi_GPIO/Raspberry%20pi%203%20GPIO_pins_v2.png)

 - GPIO stands for General Purpose Input/Output. 
 - It's a standard interface used to connect microcontrollers to other electronic devices. 
 - For example, it can be used with sensors, diodes, displays, and System-on-Chip modules.
 - It allows for providing external power supply, remote control of connected devices, broadcasting more

GPIO can be used in three modes:

 - input
 - output
 - UART interface
 




