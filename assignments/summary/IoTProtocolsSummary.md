**IIoT protocols**

**Why IIoT protocols and what is the use ?**

- IoT communication protocols are modes of communication that protect and ensure optimum security to the data being exchanged between connected devices.

**4-20mA**

- In industries the 4-20 mA loop is the dominant standard process 
- The 4-20 mA loop standard has become in the process control industry after 3-15 psi
- The main principle of 4-20 mA loop is every element in the loop either provides voltage or has a voltage drop. However, the current, "I" is the same everywhere in the loop. so 4-20 mA loop is using current as a means of conveying process information is so reliable in it.

**4-20mA** means as name suggests 
  - 4mA is below 4mA current it is difficult to measure and maintain the low loop voltage drop  
  - 20mA is high current 
This 4-20mA range represents a two-point linear scale where a 4 mA signal represents 0% of a sensor's measurement, while 20 mA indicates 100% of the measurement

**Components of a 4-20 mA Current Loop**

![ALT](https://www.predig.com/sites/default/files/images/Indicator/back_to_basics/4-20mA_Current_Loops/4-20mA_current_loop_components.jpg)

**Sensor** :-  The sensor is used to measure temperature or humidity for example if we take to measure the 50feet water tank, when the sensor is measure the 0feet of empty water then we have to translate to current in this case we use the Transmitter and same as the sensor is measure the 50feet of filled water then by using this measurement we will translate to current using the Transmitter 

**Transmitter** :- The transmitter would need to translate zero feet of empty tank to 4mA current signal and the 50feet of filled water to translate 20mA current signal

**Power Source** :- The powersupply is outputs only dc current so it will supply the current only in one direction But the powersupply voltahe must be atleast 10% greater than total voltage drop of attached Components. 

**Loop** :- It refers to the actual wire connecting the sensor to the device receiving the 4-20 mA signal and then back to the transmitter. The current signal on the loop is regulated by the transmitter according to the sensor's measurement. 

**Receiver** :- In the loop is connected to  device which can receive and interpret the current signal. This current signal must be translated into units that can be easily understood by operators.This device also needs to either display the information received (for monitoring purposes) or automatically to show the consumers.

**Pros & Cons of 4-20 mA Loops**

**Pros**

 - The 4-20 mA current loop is the dominant standard in many industries.
 - It is the simplest option to connect and configure.
 - It uses less wiring and connections than other signals, greatly reducing initial setup costs.
 - Better for traveling long distances, as current does not degrade over long connections like voltage.
 - It is less sensitive to background electrical noise.
 - Since 4 mA is equal to 0% output, it is incredibly simple to detect a fault in the system.

**Cons**
  - Current loops can only transmit one particular process signal.
  - Multiple loops must be created in situations where there are numerous process variables that require transmission.Running so much   wire could lead to problems with ground loops if independent loops are not properly isolated.
  - These isolation requirements become exponentially more complicated as the number of loops increases.

------------------------------------------------------------------------------------------------------------------------------------------
**MODBUS**

**What is modbus and it uses ?**

 - Modbus is a data communications protocol originally published by Modicon in 1979 for use with its programmable logic controllers.      
 - Modbus has become a de facto standard communication protocol and is now a commonly available means of connecting industrial electronic devices.
 - Modbus is popular in industrial environment and mainly developed for industrial applications, is relatively easy to deploy and maintain compared to other standards and also modbus is openly published and royalty-free.

**Complete overview of modbus**

![ALT](https://realpars.com/wp-content/uploads/2018/12/How-does-Modbus-Communication-Protocol-Work-Between-Devices-1.png)

 - Modbus is a serial communication protocol for use with programmable logic controllers. 
 - It is used to transmit signals from instrumentation and control devices back to a main controller; or data gathering system, for example a system that measures temperature and humidity and communicates the results to a computer, according to Simply Modbus.
 - Modbus is an Application Layer protocol.
 - The Modbus method is used for transmitting information over serial lines between electronic devices. The device requesting information is called “master” and “slaves” are the devices supplying information. In a standard Modbus network, there is one master and up to 247 slaves, each with a unique slave address from 1 to 247. 

![ALT](https://gitlab.com/balanaguyashwanth/iotmodule2/-/raw/master/assignments/summary/Assets/modbus.png)

 - This Modbus protocol is also commonly used in IoT as a local interface to manage devices.
 - Modbus protocol can be used over 2 interfaces
    - RS485 - called as Modbus RTU
    - Ethernet - called as Modbus TCP/IP

**What is RS485 ?**
 - RS485 is a form of serial communication protocol.
 - It can handle up to 32 connected devices. This allowed users in manufacturing facilities to connect larger applications and whole machines with one protocol.
 - It is less susceptible to noise issues. Electric noise can cause all kinds of problems for electronics.

**What is  Ethernet ?**
 - Ethernet simply refers to the most common type of Local Area Network (LAN) used today.
 - Manily ethernet is like wifi but is not wireless it is wired protocol, by using ethernet we can use internet to any device, if your device have an Ethernet port built-in. 
 - These cables run from your modem or modem-router combo (known as a gateway) to the Ethernet ports on your online-enabled devices, like computers, laptops, or smart TVs.  

------------------------------------------------------------------------------------------------------------------------------------------

**OPCUA**

**What is OPCUA and it uses ?**

 - Open Platform Communications United Architecture (OPC UA) is a machine to machine communication protocol for industrial automation developed by the OPC Foundation.
 - It mainly focus on communicating with industrial equipment and systems for data collection and control.
 - It is standardized and secure communication from the field device to the cloud 

![ALT](https://www.arcweb.com/sites/default/files/Images/blog-images/I4.0-SmartWorld.jpg)

------------------------------------------------------------------------------------------------------------------------------------------

**MQTT**

 - MQTT (MQ Telemetry Transport) is a lightweight messaging protocol.
 - This protocol, which uses a publish/subscribe communication pattern, is used for machine-to-machine (M2M) communication and plays an important role in the internet of things (IoT).
 - The MQTT protocol is a good choice for wireless networks. So, it’s the perfect solution for Internet of Things applications. 
 - MQTT allows you to send commands to control outputs, read and publish data from sensor nodes and much more.
 - The bidirectional capabilities of MQTT are uniquely suited to meet the demands of industrial control systems.
 - Therefore, it makes it really easy to establish a communication between multiple devices.

  ![ALT](https://gitlab.com/balanaguyashwanth/iotmodule2/-/raw/master/assignments/summary/Assets/home-automation-mqtt-example.png)

 - MQTT is a publish/subscribe protocol that allows edge-of-network devices to publish to a broker.
 - Clients connect to this broker, which then mediates communication between the two devices
 - Each device can subscribe, or register, to particular topics. When main head client publishes a message on a subscribed topic, the broker forwards the message to any client that has subscribed.

------------------------------------------------------------------------------------------------------------------------------------------
**HTTP**

 - HTTP stands for Hyper Text Transfer Protocol
 - WWW is about communication between web clients and servers
 - Communication between client computers and web servers is done by sending HTTP Requests and receiving HTTP Responses

**HTTP Request / Response**
 
 ![ALT](https://lh3.googleusercontent.com/proxy/wYLLBvTz_OKb7CkoWNUT4VlK8iaeE_FtSnYvge3QuzGA5oW_EeEQgY3Zo2r9oE8xIdtwvlDEdYDEoZsyKZSOnrTDd4T0PR5xyMgk1nfT0EXazyQ4e945SEbVjn4ptZZzyGE)

 - A client sends an HTTP request to the web 
 - An web server receives the request
 - The server runs an application to process the request
 - The server returns an HTTP response to the browser
 - The client receives the response in a browser

 They are maily 4 methods in HTTP request:-

 - GET-Retrieve the resource from the server (e.g. when visiting a page);
 - POST-Create or post a resource on the server (e.g. when submitting a form);
 - PUT/PATCH-Update the resource on the server (used by APIs);
 - DELETE-Delete the resource from the server (used by APIs).

These can perfom by using ajax with janascript using the APIs via :-
 
 - XHR - XML Http Request ( in vanilla js )
 - Jquery
 - Axios







